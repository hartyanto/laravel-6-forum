<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'hash' => \Str::random(32) . '-' . strtotime(\Carbon\Carbon::now()),
            'name' => 'Dito Sigit',
            'email' => "sigit77@gmail.com",
            'password' => Hash::make('123456789'),
        ]);

        User::create([
            'hash' => \Str::random(32) . '-' . strtotime(\Carbon\Carbon::now()),
            'name' => 'Praga Prabowo',
            'email' => "hartyanto@gmail.com",
            'password' => Hash::make('123456789'),
        ]);

        User::create([
            'hash' => \Str::random(32) . '-' . strtotime(\Carbon\Carbon::now()),
            'name' => 'Kanita Rastiti',
            'email' => "rastiti@gmail.com",
            'password' => Hash::make('123456789'),
        ]);

        User::create([
            'hash' => \Str::random(32) . '-' . strtotime(\Carbon\Carbon::now()),
            'name' => 'Agustina Vio',
            'email' => "viovio@gmail.com",
            'password' => Hash::make('123456789'),
        ]);

        User::create([
            'hash' => \Str::random(32) . '-' . strtotime(\Carbon\Carbon::now()),
            'name' => 'Rahma Kusuma',
            'email' => "rahmakusuma@gmail.com",
            'password' => Hash::make('123456789'),
        ]);
    }
}
