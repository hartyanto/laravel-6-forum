<?php

use App\Models\Forum\Tag;
use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tags = collect(['Laravel', 'React JS', 'Vue JS', 'React Native', 'Bootstrap', 'Tailwind CSS']);
        $tags->each(function ($c) {
            Tag::create([
                'name' => $c,
                'slug' => \Str::slug($c),
            ]);
        });
    }
}
