<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Forum\Reply;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(Reply::class, function (Faker $faker) {
    return [
        'hash' => strtotime(Carbon::now()) . '-' .  \Str::random(32),
        'user_id' => rand(1, 5),
        'thread_id' => rand(1, 20),
        'body' => $faker->paragraph(15),
    ];
});
