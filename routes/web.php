<?php

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');


Route::namespace('forum')->group(function () {
    Route::get('forum/filter', 'FilterController@index');

    Route::get('forum/search', 'SearchController@index')->name('threads.search');

    Route::get('forum/tags', 'TagController@index');
    Route::get('forum/{tag}', 'TagController@show')->name('tags.show');

    Route::get('forum', 'ThreadController@index')->name('threads');
    Route::get('create-new-thread', 'ThreadController@create')->name('threads.create');
    Route::post('create-new-thread', 'ThreadController@store')->name('threads.store');
    Route::get('forum/{tag}/{thread}', 'ThreadController@show')->name('threads.show');
    Route::get('forum/{tag}/{thread}/edit', 'ThreadController@edit')->name('threads.edit');
    Route::patch('forum/{tag}/{thread}/edit', 'ThreadController@update');
    Route::delete('forum/{tag}/{thread}', 'ThreadController@destroy')->name('threads.delete');

    Route::post('reply-on-the-thread/{thread}', 'ReplyController@store')->name('replies.store');
    Route::get('edit-reply-on-the-thread/{thread}/{reply}', 'ReplyController@edit')->name('replies.edit');
    Route::patch('edit-reply-on-the-thread/{thread}/{reply}', 'ReplyController@update');

    Route::patch('mark-as-answer-at-reply/{reply}', 'AnswerController@store')->name('answer.create');
});

Route::get('{user}', 'UserController@show')->name('users.show');
Route::get('account/profile', 'UserController@edit')->name('users.edit');
Route::patch('account/profile', 'UserController@update');
