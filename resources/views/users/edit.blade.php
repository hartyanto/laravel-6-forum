@extends('layouts.partials.auth')

@section('title', 'Edit Profiles')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit profile</div>
                <div class="card-body">
                    <form action="{{ route('users.edit') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                        <div class="d-flex mb-2">
                            <img width="100" height="100" src="{{ asset(auth()->user()->avatar()) }}" class="rounded-circle mr-3" style="object-fit: cover; object-position: center;" alt="...">
                            <div class="form-group align-items-center d-flex">
                                <div>
                                    <label for="avatar">Upload avatar</label>
                                    <input type="file" name="avatar" id="avatar" class="form-control-file">
                                    @error('avatar')
                                        <small class="text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" name="username" id="username" class="form-control" placeholder="@johndoe" value="{{ old('username') ?? auth()->user()->username }}">
                            @error('username')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="name">Full name</label>
                            <input type="text" name="name" id="name" class="form-control" value="{{ old('name') ?? auth()->user()->name }}">
                            @error('name')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
