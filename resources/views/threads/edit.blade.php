@extends('layouts.app')

@section('title')
    Edit thread : {{ $thread->title}}
@endsection

@section('content')
    <div class="card">
        <div class="card-header">Edit thread</div>
        <div class="card-body">
            <form action="{{ route('threads.edit', [$thread->tag, $thread]) }}" method="post">
                @method("PATCH")
                @csrf
                @include('threads.partials.form', ['submit' => 'Edit'])
            </form>
        </div>
    </div>
@endsection