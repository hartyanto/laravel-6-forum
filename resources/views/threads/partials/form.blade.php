<div class="form-group">
    <label for="title">Title</label>
    <input type="text" value="{{ old('title') ?? $thread->title }}" name="title" id="title" class="form-control">
    @error('title')
        <small class="text-danger">{{ $message }}</small>
    @enderror
</div>
<div class="form-group">
    <label for="tag">Tag</label>
    <select name="tag" id="tag" class="form-control">
        <option disabled selected>Choose One</option>
        @foreach ($tags as $tag)
            <option {{ $tag->id == $thread->tag_id  ? 'selected' : ''}} value="{{ $tag->id }}">{{ $tag->name }}</option>
        @endforeach
    </select>
    @error('tag_id')
        <small class="text-danger">{{ $message }}</small>
    @enderror
</div>
<div class="form-group">
    <label for="body">Body</label>
    <textarea name="body" id="body" rows="10" class="form-control">{{ old('body') ?? $thread->body }}</textarea>
    @error('body')
        <small class="text-danger">{{ $message }}</small>
    @enderror
</div>
<button type="submit" class="btn btn-primary">{{ $submit }}</button>