@extends('layouts.app')

@section('title', 'All Threads')

@section('content')
<form action="{{ route('threads.search') }}" method="get">
    <input type="search" name="query" class="mb-2 form-control" placeholder="Search threads..." autocomplete="off">
</form>
<div class="card">
    <div class="card-header">
        <a href="{{ route('threads') }}" class="text-secondary">Forum</a> @isset( $tag )
            / {{ $tag->name }}
        @endisset
    </div>

    <div class="card-body">
        @forelse ($threads as $thread)
        <div class="media mb-2">
            <img width="50" height="50" src="{{ $thread->author->avatar() }}" class="rounded-circle mr-3" style="object-fit: cover; object-position: center;" alt="...">
            <div class="media-body">
                <a href="{{ route('threads.show', [$thread->tag, $thread]) }}" class="mt-0 d-block">{{ $thread->title }}</a>
                <small>in <a href="{{ route('tags.show', $thread->tag) }}">{{ $thread->tag->name }}</a></small>
                <div class="text-secondary">
                    {{ Str::limit($thread->body, 170) }}
                </div>
                <small class="text-secondary">
                    <a href="" class="text-secondary">{{ $thread->author->name }}</a> posted {{ $thread->created_at->diffForHumans() }}
                </small>
            </div>
        </div>
        <hr>
        @empty
            We cant find what are you looking for...
        @endforelse
    </div>
</div>
<div class="mt-3 justify-content-center">
    {{ $threads->appends($query)->links() }}
</div>
@endsection