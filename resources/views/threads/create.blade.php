@extends('layouts.app')

@section('title', 'Forum | Create new thread')

@section('content')
    <div class="card">
        <div class="card-header">Create new thread</div>
        <div class="card-body">
            <form action="{{ route('threads.store') }}" method="post">
                @csrf
                @include('threads.partials.form', ['submit' => 'Post'])
            </form>
        </div>
    </div>
@endsection