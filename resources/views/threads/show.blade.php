@extends('layouts.app')

@section('title', $thread->title)

@section('content')
    <div class="card{{ count($replies) >= 1 ? ' pb-3' : ''}}">
        <div class="card-header d-flex justify-content-between">
            <div>
                <h5 class="text-secondary d-block">{{ $thread->title }}</h5>
                <small class="mt-0 d-block">in <a href="{{ route('tags.show', $thread->tag) }}">{{ $thread->tag->name }}</a></small>
            </div>
            <div class="d-flex align-items-center">
                @can('update', $thread)
                <div class="d-flex">
                    <a href="{{ route('threads.edit', [$thread->tag, $thread]) }}" class="btn btn-success mr-2">Edit Thread</a>
                    <form action="{{ route('threads.delete', [$thread->tag, $thread]) }}" method="post">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </div>
                @endcan
            </div>
        </div>

        <div class="card-body">
            <div class="media">
                <img width="50" height="50" src="{{ asset($thread->author->avatar()) }}" class="rounded-circle mr-3" style="object-fit: cover; object-position: center;" alt="...">
                <div class="media-body">
                    
                    <div class="text-secondary">
                        {!! nl2br($thread->body) !!}
                    </div>
                    <small class="text-secondary">
                        Written by <a href="{{ route('users.show', $thread->author->usernameOrHash()) }}" class="text-secondary">{{ $thread->author->name }}</a>, {{ $thread->created_at == $thread->updated_at ? 'posted ' . $thread->created_at->diffForHumans() : 'edited ' . $thread->updated_at->diffForHumans() }}
                    </small>
                </div>
            </div>
        </div>
    </div>

    @if ( $thread->answer )
        <div class="row justify-content-end mr-3" style="margin-top: -2em">
            <div class="col-md-11">
                <div class="card mb-5">
                    <div class="card-header">
                        Best Answer
                    </div>
                    <div class="card-body">   
                        <div class="media">
                            <img width="50" height="50" src="{{ asset($thread->answer->user->avatar()) }}" class="rounded-circle mr-3" style="object-fit: cover; object-position: center;" alt="...">
                            <div class="media-body">
                                <div class="d-flex justify-content-between">
                                    <a href=""><h5 class="mt-0 d-block">{{ $thread->answer->user->name }}</h5></a>
                                </div>
                                <div class="text-secondary">
                                    {!! $thread->answer->body !!}
                                </div>
                                <small class="text-secondary d-flex justify-content-between">
                                    <div>
                                        Answered at {{ $thread->answer->created_at->diffForHumans() }}
                                    </div>
                                    <div>
                                        &middot; <a href="" class="text-success">Mark as answer</a>
                                    </div>
                                </small>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @include('replies.index')
    @include('replies.create')
@endsection