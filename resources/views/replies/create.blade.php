@auth    
    <div class="mt-3">
        <div class="media">
            <img width="50" height="50" src="{{ asset(auth()->user()->avatar()) }}" class="rounded-circle mr-3" style="object-fit: cover; object-position: center;" alt="...">
            <div class="media-body">
                <div class="mt-0 mb-1 d-block">{{ auth()->user()->name }}</div>
                <form class="mb-2" action="{{ route('replies.store', $thread) }}" method="POST">
                    @csrf
                    <textarea style="resize: none" class="form-control" name="body" id="body" rows="5" placeholder="Write a comment...."></textarea>
                    <button type="submit" class="mt-2 btn btn-primary">Reply</button>
                </form>
            </div>
        </div>
    </div>
@else
<div class="text-center py-5 text-secondary">
    Please <a href="{{ route('register') }}">register</a> or <a href="{{ route('login') }}">login</a> to reply thread.
</div>
@endauth