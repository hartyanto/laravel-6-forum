@if (count($replies) >= 1)
    <div class="row justify-content-end mr-3" style="margin-top: -2em">
        <div class="col-md-11">
            <div class="card">
                <div class="card-body">
                    @foreach ($replies as $reply)    
                    <div class="media">
                        <img width="50" height="50" src="{{ asset($reply->user->avatar()) }}" class="rounded-circle mr-3" style="object-fit: cover; object-position: center;" alt="...">
                        <div class="media-body">
                            <div class="d-flex justify-content-between">
                                <a href=""><h5 class="mt-0 d-block">{{ $reply->user->name }}</h5></a>
                                @can('update', $reply)
                                    <a href="{{ route('replies.edit', [$thread, $reply]) }}"  class=""><small>Edit</small></a>
                                @endcan
                            </div>
                            <div class="text-secondary">
                                {{ $reply->body }}
                            </div>
                            <small class="text-secondary d-flex justify-content-between">
                                <div>
                                    Replied {{ $reply->created_at->diffForHumans() }}{!! $reply->created_at == $reply->updated_at ? '' : ' &middot; <span class="font-italic"> edited ' . $reply->updated_at->diffForHumans() . '</span>' !!}
                                </div>
                                @can('update', $thread)
                                    <div>
                                        &middot; <a href="{{ route('answer.create', $reply) }}" class="text-success"
                                        onclick="event.preventDefault(); document.getElementById('mark-as-answer-{{ $reply->hash }}').submit();">Mark as answer</a>
                                    </div>
                                    
                                    <form action="{{ route('answer.create', $reply) }}" method="post" id="mark-as-answer-{{ $reply->hash }}" style="display: none;">
                                        @csrf
                                        @method('PATCH')
                                    </form>
                                @endcan
                            </small>
                        </div>
                    </div>
                    <hr>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endif