@extends('layouts.app')

@section('title', $thread->title)

@section('content')
    <div class="card">
        <div class="card-header d-flex justify-content-between">
            <div>
                <h5 class="text-secondary d-block">{{ $thread->title }}</h5>
                <small class="mt-0 d-block">in <a href="">{{ $thread->tag->name }}</a></small>
            </div>
            <div class="d-flex align-items-center">
                @if ( $thread->user_id == Auth::id())
                    <a href="{{ route('threads.edit', [$thread->tag, $thread]) }}" class="btn btn-success">Edit Thread</a>
                @endif
            </div>
        </div>

        <div class="card-body">
            <div class="media">
                <img width="50" height="50" src="{{ $thread->author->avatar() }}" class="rounded-circle mr-3" style="object-fit: cover; object-position: center;" alt="...">
                <div class="media-body">
                    
                    <div class="text-secondary">
                        {!! nl2br($thread->body) !!}
                    </div>
                    <small class="text-secondary">
                        Written by <a href="" class="text-secondary">{{ $thread->author->name }}</a>, {{ $thread->created_at == $thread->updated_at ? 'posted ' . $thread->created_at->diffForHumans() : 'edited ' . $thread->updated_at->diffForHumans() }}
                    </small>
                </div>
            </div>
        </div>
    </div>

    @auth    
        <div class="mt-3">
            <div class="media">
                <img width="50" height="50" src="{{ auth()->user()->avatar() }}" class="rounded-circle mr-3" style="object-fit: cover; object-position: center;" alt="...">
                <div class="media-body">
                    <div class="mt-0 mb-1 d-block">{{ auth()->user()->name }}</div>
                    <form class="mb-2" action="{{ route('replies.edit', [$thread, $reply]) }}" method="POST">
                        @csrf
                        @method('PATCH')
                        <textarea style="resize: none" class="form-control" name="body" id="body" rows="5" placeholder="Write a comment....">{{ old('body') ?? $reply->body }}</textarea>
                        <button type="submit" class="mt-2 btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    @else
    <div class="text-center py-5 text-secondary">
        Please <a href="{{ route('register') }}">register</a> or <a href="{{ route('login') }}">login</a> to reply thread.
    </div>
    @endauth
@endsection