<div class="col-md-3">
    <div class="list-group mb-2">
        @auth
            <a href="{{ route("threads.create") }}" class="list-group-item list-group-item-action{{ request()->route()->named('threads.create') ? ' active' : '' }}">New Thread</a>                                 
            <a href="/forum/filter?by=me" class="list-group-item list-group-item-action{{ request()->query('by') == 'me' ? ' active' : '' }}">My Threads</a> 
        @endauth
        <a href="/forum/filter?by=popular" class="list-group-item list-group-item-action{{ request()->query('by') == 'popular' ? ' active' : '' }}">Popular</a> 
        <a href="/forum/filter?by=answered" class="list-group-item list-group-item-action{{ request()->query('by') == 'answered' ? ' active' : '' }}">Answered</a> 
        <a href="/forum/filter?by=unanswered" class="list-group-item list-group-item-action{{ request()->query('by') == 'unanswered' ? ' active' : '' }}">Unanswered</a> 
    </div>
    <tags></tags>
</div>