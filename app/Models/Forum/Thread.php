<?php

namespace App\Models\Forum;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;
use App\User;

class Thread extends Model
{
    use Searchable;
    protected $fillable = ["title", "slug", "tag_id", "body"];
    protected $with = ['tag', 'author'];
    protected $withCount = ['replies'];

    public function searchableAs()
    {
        return 'threads';
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function author()
    {
        return $this->belongsTo(User::class, "user_id");
    }

    public function tag()
    {
        return $this->belongsTo(Tag::class);
    }

    public function replies()
    {
        return $this->hasMany(Reply::class);
    }

    public function answer()
    {
        return $this->belongsTo(Reply::class, 'reply_id');
    }
}
