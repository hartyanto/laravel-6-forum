<?php

namespace App\Http\Controllers\Forum;

use App\Http\Controllers\Controller;
use App\Http\Requests\Forum\ThreadRequest;
use App\Models\Forum\Reply;
use App\Models\Forum\Tag;
use App\Models\Forum\Thread;
use Illuminate\Http\Request;

class ThreadController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show');
    }

    public function index()
    {
        $threads = Thread::latest()->simplePaginate(10);
        return view('threads.index', compact('threads'));
    }

    public function show(Tag $tag, Thread $thread)
    {
        $replies = $thread->replies;
        return view('threads.show', compact('tag', 'thread', 'replies'));
    }

    public function create()
    {
        $thread = new Thread;
        return view('threads.create', compact('thread'));
    }

    public function store(ThreadRequest $request)
    {
        $slug = \Str::slug(request('title'));

        if (Thread::where('slug', $slug)->first()) {
            $slug = \Str::slug(request('title')) . '-' .  \Str::random(6);
        }

        $thread = auth()->user()->threads()->create([
            'title' => request('title'),
            'slug' => $slug,
            'tag_id' => request('tag'),
            'body' => request('body')
        ]);

        return redirect()->route('threads.show', [$thread->tag, $thread]);
    }

    public function edit(Tag $tag, Thread $thread)
    {
        return view('threads.edit', compact('thread'));
    }

    public function update(Tag $tag, Thread $thread, ThreadRequest $request)
    {
        $this->authorize('update', $thread);

        $thread->update([
            'title' => request('title'),
            'tag_id' => request('tag'),
            'body' => request('body')
        ]);

        return redirect()->route('threads.show', [$thread->tag, $thread]);
    }

    public function destroy(Tag $tag, Thread $thread)
    {
        $thread->delete();
        return redirect()->route('threads');
    }
}
