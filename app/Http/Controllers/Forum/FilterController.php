<?php

namespace App\Http\Controllers\Forum;

use App\Http\Controllers\Controller;
use App\Models\Forum\Thread;
use Illuminate\Http\Request;

class FilterController extends Controller
{
    public function index()
    {
        $query = request('by');

        switch ($query) {
            case $query == 'popular';
                $threads = Thread::whereHas('replies')->orderBy('replies_count', 'DESC')->paginate(10);
                break;
            case $query == 'me';
                if (auth()->check()) {
                    $threads = auth()->user()->threads()->latest()->simplePaginate();
                } else {
                    return redirect()->route('login');
                }
                break;
            case $query == 'answered';
                $threads = Thread::whereNotNull('reply_id')->latest()->simplePaginate(10);
                break;
            case $query == 'unanswered';
                $threads = Thread::doesntHave('replies')->latest()->simplePaginate(10);
                break;

            default:
                return redirect()->routes('threads');
                break;
        }

        return view('threads.index', compact('threads', 'query'));
    }
}
