<?php

namespace App\Http\Controllers\Forum;

use App\Http\Controllers\Controller;
use App\Http\Requests\Forum\ReplyRequest;
use App\Models\Forum\Reply;
use App\Models\Forum\Thread;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ReplyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(ReplyRequest $request, Thread $thread)
    {
        $hash = strtotime(Carbon::now()) . '-' .  \Str::random(32);
        auth()->user()->replies()->create([
            'hash' => $hash,
            'body' => request('body'),
            'thread_id' => $thread->id,
        ]);

        return back();
    }

    public function edit(Thread $thread, Reply $reply)
    {
        return view('replies.edit', compact('thread', 'reply'));
    }

    public function update(ReplyRequest $request, Thread $thread, Reply $reply)
    {
        $this->authorize('update', $reply);

        $reply->update([
            'body' => request('body'),
        ]);

        return redirect()->route('threads.show', [$thread->tag, $thread]);
    }
}
