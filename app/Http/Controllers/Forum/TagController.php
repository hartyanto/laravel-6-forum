<?php

namespace App\Http\Controllers\Forum;

use App\Http\Controllers\Controller;
use App\Models\Forum\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    public function show(Tag $tag)
    {
        $threads = $tag->threads()->latest()->simplePaginate(10);
        return view('threads.index', compact('threads', 'tag'));
    }

    public function index()
    {
        return Tag::get();
    }
}
