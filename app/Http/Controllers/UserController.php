<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('show');
    }

    public function show($user)
    {
        $user = User::withCount('threads')->whereHash($user)->orWhere('username', $user)->firstOrFail();
        $threads = $user->threads()->latest()->simplePaginate(5);
        return view('users.show', compact('user', 'threads'));
    }

    public function edit()
    {
        return view('users.edit');
    }

    public function update()
    {
        $avatar = request()->file('avatar');
        $avatar_validate = ['image', 'mimes:jpeg,png,jpg,svg', 'max:2048'];

        request()->validate([
            'username' => ['required', 'alpha_num', 'max:20', 'min:3', 'unique:users,username,' . auth()->id()],
            'name' => ['required', 'string'],
            'avatar' => $avatar ? $avatar_validate : '',
        ]);

        $hash = auth()->user()->hash;
        $avatar_name = $avatar->storeAs('profile-picture', "{$hash}.{$avatar->extension()}");

        auth()->user()->update([
            'username' => request('username'),
            'name' => request('name'),
            'avatar' => $avatar_name,
        ]);

        return back();
    }
}
